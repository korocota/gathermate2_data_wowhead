#!/usr/bin/env python3
import requests
import re
import json
from dataclasses import dataclass
import typing
import math


@dataclass
class WowheadObject:
    name: str
    ids: typing.List[str]
    coordinates: dict
    gathermate_id: str

    def __init__(self, name: str, ids: typing.List[str], gathermate_id: str):
        self.name = name
        self.ids = ids
        self.coordinates = dict()
        self.gathermate_id = gathermate_id

        for object_id in self.ids:
            result = requests.get(f'https://tbc.wowhead.com/object={object_id}')
            data = re.search(r'var g_mapperData = (.*);', result.text)
            try:
                data_parsed = json.loads(data.group(1))
            except AttributeError:
                print(f"No locations for {object_id} ({self.name})")
                continue
            for zone in data_parsed:
                wow_zone = WOWHEAD_ZONE_MAP.get(zone)
                if wow_zone is None:
                    print(f"Found unlisted zone: {zone}")
                    continue
                coords = list()
                for coord in data_parsed[zone][0]["coords"]:
                    coords.append(Coordinate(coord[0], coord[1]))
                if self.coordinates.get(wow_zone) is None:
                    self.coordinates[wow_zone] = coords
                else:
                    self.coordinates[wow_zone] += coords
        print(f"Finished processing {self.name}")


@dataclass(eq=True, frozen=True)
class Zone:
    name: str
    id: str


@dataclass
class Coordinate:
    x: float
    y: float
    coord: int = 0

    def __repr__(self):
        return str(self.as_gatherer_coord())

    def as_gatherer_coord(self):
        if self.coord == 0:
          self.coord = math.floor((self.x/100)*10000+0.5)*1000000+math.floor((self.y/100)*10000+0.5)*100
        return self.coord


@dataclass
class GathererEntry:
    coordinate: Coordinate
    entry_id: str

    def __repr__(self):
        return f"		[{self.coordinate}] = {self.entry_id},"

    def __lt__(self, other):
        return self.coordinate.as_gatherer_coord() < other.coordinate.as_gatherer_coord()


@dataclass
class GathererZone:
    zone: Zone
    entries: typing.List[GathererEntry]

    def __repr__(self):
        output = f'	[{self.zone.id}] = {{\n'
        for entry in sorted(self.entries):
            output += f'{str(entry)}\n'
        output += '	},\n'
        return output

    def __lt__(self, other):
        return int(self.zone.id) < int(other.zone.id)


@dataclass
class Aggregate:
    type: str
    zones: typing.List[GathererZone]

    def __init__(self, type, objects):
        self.type = type
        self.zones = []
        for object in objects:
            for zone in object.coordinates:
                for coord in object.coordinates[zone]:
                    self.add(zone, GathererEntry(coord, object.gathermate_id))

    def __repr__(self):
        output = f"GatherMateData2{self.type}DB = {{\n"
        for zone in sorted(self.zones):
            output += f'{str(zone)}'
        output += '}'
        return output

    def add(self, zone: Zone, entry: GathererEntry):
        for gatherer_zone in self.zones:
            if gatherer_zone.zone == zone:
                while entry.coordinate in [x.coordinate for x in gatherer_zone.entries]:
                  entry.coordinate.coord = entry.coordinate.as_gatherer_coord() + 1
                gatherer_zone.entries.append(entry)
                return
        self.zones.append(GathererZone(zone, [entry]))


# key is zone id as it appears in wowhead
# GathererEntry is the id from `/dump C_Map.GetBestMapForUnit("player");` while in the zone
WOWHEAD_ZONE_MAP = {
    '14': Zone("Durotar", "1411"),
    '215': Zone("Mulgore", "1412"),
    '17': Zone("The Barrens", "1413"),
    '36': Zone("Alterac Mountains", "1416"),
    '45': Zone("Arathi Highlands", "1417"),
    '3': Zone("Badlands", "1418"),
    '4': Zone("Blasted Lands", "1419"),
    '85': Zone("Tirisfal Glades", "1420"),
    '130': Zone("Silverpine Forest", "1421"),
    '28': Zone("Western Plaguelands", "1422"),
    '139': Zone("Eastern Plaguelands", "1423"),
    '267': Zone("Hillsbrad Foothills", "1424"),
    '47': Zone("The Hinterlands", "1425"),
    '1': Zone("Dun Morogh", "1426"),
    '51': Zone("Searing Gorge", "1427"),
    '46': Zone("Burning Steppes", "1428"),
    '12': Zone("Elwynn Forest", "1429"),
    '41': Zone("Deadwind Pass", "1430"),
    '10': Zone("Duskwood", "1431"),
    '38': Zone("Loch Modan", "1432"),
    '44': Zone("Redridge Mountains", "1433"),
    '33': Zone("Stranglethorn Vale", "1434"),
    '8': Zone("Swamp of Sorrows", "1435"),
    '40': Zone("Westfall", "1436"),
    '11': Zone("Wetlands", "1437"),
    '141': Zone("Teldrassil", "1438"),
    '148': Zone("Darkshore", "1439"),
    '331': Zone("Ashenvale", "1440"),
    '400': Zone("Thousand Needles", "1441"),
    '406': Zone("Stonetalon Mountains", "1442"),
    '405': Zone("Desolace", "1443"),
    '357': Zone("Feralas", "1444"),
    '15': Zone("Dustwallow Marsh", "1445"),
    '440': Zone("Tanaris", "1446"),
    '16': Zone("Azshara", "1447"),
    '361': Zone("Felwood", "1448"),
    '490': Zone("Un'Goro Crater", "1449"),
    '493': Zone("Moonglade", "1450"),
    '1377': Zone("Silithus", "1451"),
    '618': Zone("Winterspring", "1452"),
    '3430': Zone("Eversong Woods", "1941"),
    '3433': Zone("Ghostlands", "1942"),
    '3524': Zone("Azuremyst Isle", "1943"),
    '3525': Zone("Bloodmyst Isle", "1950"),
    '3483': Zone("Hellfire Peninsula", "1944"),
    '3518': Zone("Nagrand", "1951"),
    '3519': Zone("Terokkar Forest", "1952"),
    '3520': Zone("Shadowmoon Valley", "1948"),
    '3521': Zone("Zangarmarsh", "1946"),
    '3522': Zone("Blade's Edge Mountains", "1949"),
    '3523': Zone("Netherstorm", "1953"),
}

HERBS = [
    WowheadObject(name="Peacebloom", ids=['1618', '3724'], gathermate_id='401'),
    WowheadObject(name="Silverleaf", ids=['1617', '3725'], gathermate_id='402'),
    WowheadObject(name="Earthroot", ids=['1619', '3726'], gathermate_id='403'),
    WowheadObject(name="Mageroyal", ids=['1620', '3727'], gathermate_id='404'),
    WowheadObject(name="Briarthorn", ids=['1621', '3729'], gathermate_id='405'),
    WowheadObject(name="Swiftthistle", ids=[], gathermate_id='406'),
    WowheadObject(name="Stranglekelp", ids=['2045'], gathermate_id='407'),
    WowheadObject(name="Bruiseweed", ids=['1622', '3730'], gathermate_id='408'),
    WowheadObject(name="Wild Steelbloom", ids=['1623'], gathermate_id='409'),
    WowheadObject(name="Grave Moss", ids=['1628'], gathermate_id='410'),
    WowheadObject(name="Kingsblood", ids=['1624'], gathermate_id='411'),
    WowheadObject(name="Liferoot", ids=['2041'], gathermate_id='412'),
    WowheadObject(name="Fadeleaf", ids=['2042'], gathermate_id='413'),
    WowheadObject(name="Goldthorn", ids=['2046'], gathermate_id='414'),
    WowheadObject(name="Khadgar's Whisker", ids=['2043'], gathermate_id='415'),
    WowheadObject(name="Wintersbite", ids=['2044'], gathermate_id='416'),
    WowheadObject(name="Firebloom", ids=['2866'], gathermate_id='417'),
    WowheadObject(name="Purple Lotus", ids=['142140'], gathermate_id='418'),
    WowheadObject(name="Wildvine", ids=[], gathermate_id='419'),
    WowheadObject(name="Arthas' Tears", ids=['142141', '176642'], gathermate_id='420'),
    WowheadObject(name="Sungrass", ids=['142142', '176636'], gathermate_id='421'),
    WowheadObject(name="Blindweed", ids=['142143'], gathermate_id='422'),
    WowheadObject(name="Ghost Mushroom", ids=['142144'], gathermate_id='423'),
    WowheadObject(name="Gromsblood", ids=['142145', '176637'], gathermate_id='424'),
    WowheadObject(name="Golden Sansam", ids=['176583', '176638'], gathermate_id='425'),
    WowheadObject(name="Dreamfoil", ids=['176584', '176639'], gathermate_id='426'),
    WowheadObject(name="Mountain Silversage", ids=['176586', '176640'], gathermate_id='427'),
    WowheadObject(name="Plaguebloom", ids=['176587', '176641'], gathermate_id='428'),
    WowheadObject(name="Icecap", ids=['176588'], gathermate_id='429'),
    WowheadObject(name="Bloodvine", ids=[], gathermate_id='430'),
    WowheadObject(name="Black Lotus", ids=['176589'], gathermate_id='431'),
    WowheadObject(name="Felweed", ids=['183044', '181270'], gathermate_id='432'),
    WowheadObject(name="Dreaming Glory", ids=['183045','181271'], gathermate_id='433'),
    WowheadObject(name="Terocone", ids=['181277'], gathermate_id='434'),
    WowheadObject(name="Ancient Lichen", ids=['181278'], gathermate_id='435'),
    WowheadObject(name="Bloodthistle", ids=['181166'], gathermate_id='436'),
    WowheadObject(name="Mana Thistle", ids=['181281'], gathermate_id='437'),
    WowheadObject(name="Netherbloom", ids=['181279'], gathermate_id='438'),
    WowheadObject(name="Nightmare Vine", ids=['181280'], gathermate_id='439'),
    WowheadObject(name="Ragveil", ids=['183043','181275'], gathermate_id='440'),
    WowheadObject(name="Flame Cap", ids=['181276'], gathermate_id='441'),
]

ORES = [
    WowheadObject(name="Copper Vein", ids=['1731', '3763', '103713', '2055', '103714'], gathermate_id='201'),
    WowheadObject(name="Tin Vein", ids=['1732', '3764', '2054', '103711', '103709'], gathermate_id='202'),
    WowheadObject(name="Iron Deposit", ids=['1735', '103710', '103712'], gathermate_id='203'),
    WowheadObject(name="Silver Vein", ids=['1733', '105569'], gathermate_id='204'),
    WowheadObject(name="Gold Vein", ids=['1734', '181109', '150080'], gathermate_id='205'),
    WowheadObject(name="Mithril Deposit", ids=['2040', '176645', '150079'], gathermate_id='206'),
    WowheadObject(name="Ooze Covered Mithril Deposit", ids=['123310'], gathermate_id='207'),
    WowheadObject(name="Truesilver Deposit", ids=['2047', '150081', '181108'], gathermate_id='208'),
    WowheadObject(name="Ooze Covered Silver Vein", ids=['73940'], gathermate_id='209'),
    WowheadObject(name="Ooze Covered Gold Vein", ids=['73941'], gathermate_id='210'),
    WowheadObject(name="Ooze Covered Truesilver Deposit", ids=['123309'], gathermate_id='211'),
    WowheadObject(name="Ooze Covered Rich Thorium Vein", ids=['177388'], gathermate_id='212'),
    WowheadObject(name="Ooze Covered Thorium Vein", ids=['123848'], gathermate_id='213'),
    WowheadObject(name="Small Thorium Vein", ids=['324', '176643', '150082'], gathermate_id='214'),
    WowheadObject(name="Rich Thorium Vein", ids=['175404', '176644'], gathermate_id='215'),
    WowheadObject(name="Dark Iron Deposit", ids=['165658'], gathermate_id='217'),
    WowheadObject(name="Lesser Bloodstone Deposit", ids=['2653'], gathermate_id='218'),
    WowheadObject(name="Incendicite Mineral Vein", ids=['1610', '1667'], gathermate_id='219'),
    WowheadObject(name="Indurium Mineral Vein", ids=['19903'], gathermate_id='220'),
    WowheadObject(name="Fel Iron Deposit", ids=['181555'], gathermate_id='221'),
    WowheadObject(name="Adamantite Deposit", ids=['181556'], gathermate_id='222'),
    WowheadObject(name="Rich Adamantite Deposit", ids=['181569', '181570'], gathermate_id='223'),
    WowheadObject(name="Khorium Vein", ids=['181557'], gathermate_id='224'),
]

TREASURES = [
    WowheadObject(name="Giant Clam", ids=['2744', '19017', '19018', '179244'], gathermate_id='501'),
    WowheadObject(name="Battered Chest", ids=['2843', '106319', '106318', '2849'], gathermate_id='502'),
    WowheadObject(name="Tattered Chest", ids=['2845', '105571', '2846', '2847', '2844'], gathermate_id='503'),
    WowheadObject(name="Solid Chest", ids=['2850', '4149', '153453', '2857', '153451', '153454', '2855', '2852'], gathermate_id='504'),
    WowheadObject(name="Large Iron Bound Chest", ids=['74447', '75297', '75296', '75295'], gathermate_id='505'),
    WowheadObject(name="Large Solid Chest", ids=['74448', '75300', '153464', '153463', '153462', '75298', '75299', '153461'], gathermate_id='506'),
    WowheadObject(name="Large Battered Chest", ids=['75293'], gathermate_id='507'),
    WowheadObject(name="Buccaneer's Strongbox", ids=['123330', '123331', '123333', '123332'], gathermate_id='508'),
    WowheadObject(name="Large Mithril Bound Chest", ids=['153468', '131978', '153469', '153465'], gathermate_id='509'),
    WowheadObject(name="Large Darkwood Chest", ids=['131979'], gathermate_id='510'),
    WowheadObject(name="Un'Goro Dirt Pile", ids=['157936'], gathermate_id='511'),
    WowheadObject(name="Bloodpetal Sprout", ids=['164958'], gathermate_id='512'),
    WowheadObject(name="Blood of Heroes", ids=['176213'], gathermate_id='513'),
    WowheadObject(name="Practice Lockbox", ids=['178244', '178245', '178246'], gathermate_id='514'),
    WowheadObject(name="Battered Footlocker", ids=['179488', '179490', '179486'], gathermate_id='515'),
    WowheadObject(name="Waterlogged Footlocker", ids=['179487', '179491', '179489'], gathermate_id='516'),
    WowheadObject(name="Dented Footlocker", ids=['179492', '179494', '179496'], gathermate_id='517'),
    WowheadObject(name="Mossy Footlocker", ids=['179493', '179497', '179495'], gathermate_id='518'),
    WowheadObject(name="Scarlet Footlocker", ids=['179498'], gathermate_id='519'),
    WowheadObject(name="Burial Chest", ids=['181665'], gathermate_id='520'),
    WowheadObject(name="Fel Iron Chest", ids=['181798'], gathermate_id='521'),
    WowheadObject(name="Heavy Fel Iron Chest", ids=['181800'], gathermate_id='522'),
    WowheadObject(name="Adamantite Bound Chest", ids=['181802'], gathermate_id='523'),
    WowheadObject(name="Felsteel Chest", ids=['181804'], gathermate_id='524'),
    WowheadObject(name="Glowcap", ids=['182053'], gathermate_id='525'),
    WowheadObject(name="Wicker Chest", ids=['184740'], gathermate_id='526'),
    WowheadObject(name="Primitive Chest", ids=['184793'], gathermate_id='527'),
    WowheadObject(name="Solid Fel Iron Chest", ids=['184930','184933','184935'], gathermate_id='528'),
    WowheadObject(name="Bound Fel Iron Chest", ids=['184931','184932','184934'], gathermate_id='529'),
    WowheadObject(name="Bound Adamantite Chest", ids=['184938','184936','184940'], gathermate_id='530'),
    # WowheadObject(name="Netherwing Egg", ids=[], gathermate_id='531'),  # No object in wowhead yet
]

FISHES = [
    WowheadObject(name="Floating Wreckage", ids=['180685', '180901', '180662', '180751'], gathermate_id='101'),
    WowheadObject(name="Patch of Elemental Water", ids=['180753'], gathermate_id='102'),
    WowheadObject(name="Floating Debris", ids=['180655'], gathermate_id='103'),
    WowheadObject(name="Oil Spill", ids=['180661'], gathermate_id='104'),
    WowheadObject(name="Firefin Snapper School", ids=['180683', '180752', '180657', '180902'], gathermate_id='105'),
    WowheadObject(name="Greater Sagefish School", ids=['180684'], gathermate_id='106'),
    WowheadObject(name="Oily Blackmouth School", ids=['180682', '180900', '180664', '180582', '180750'], gathermate_id='107'),
    WowheadObject(name="Sagefish School", ids=['180656', '180663'], gathermate_id='108'),
    WowheadObject(name="School of Deviate Fish", ids=['180658'], gathermate_id='109'),
    WowheadObject(name="Stonescale Eel Swarm", ids=['180712'], gathermate_id='110'),
    WowheadObject(name="Highland Mixed School", ids=['182957'], gathermate_id='112'),
    WowheadObject(name="Pure Water", ids=['182951'], gathermate_id='113'),
    WowheadObject(name="Bluefish School", ids=['182959'], gathermate_id='114'),
    WowheadObject(name="Brackish Mixed School", ids=['182954'], gathermate_id='115'),
    WowheadObject(name="Mudfish School", ids=['182958'], gathermate_id='116'),
    WowheadObject(name="School of Darter", ids=['182956'], gathermate_id='117'),
    WowheadObject(name="Sporefish School", ids=['182953'], gathermate_id='118'),
    WowheadObject(name="Steam Pump Flotsam", ids=['182952'], gathermate_id='119'),
    WowheadObject(name="School of Tastyfish", ids=['180248'], gathermate_id='120'),
]

if __name__ == '__main__':
    with open("GatherMate2_Data/HerbalismData.lua", "w") as file:
        print(Aggregate("Herb", HERBS), file=file)
    with open("GatherMate2_Data/MiningData.lua", "w") as file:
        print(Aggregate("Mine", ORES), file=file)
    with open("GatherMate2_Data/TreasureData.lua", "w") as file:
        print(Aggregate("Treasure", TREASURES), file=file)
    with open("GatherMate2_Data/FishData.lua", "w") as file:
        print(Aggregate("Fish", FISHES), file=file)
